import tap from 'tap'
import { addMoney, isNumber, isTwoCents, validMoney } from '../mny.js'

tap.test("addMoney adds correctly", t => {
  const total1 = addMoney('$0.45', 1, 2)
  t.match(total1, 3.45)

  const total2 = addMoney('$0.45', 1, "bazinga")
  t.match(total2, 1.45)
  t.end()
})

tap.test("isTwoCents checks correctly", t => {
  t.match(false, isTwoCents(2))
  t.match(false, isTwoCents("23"))
  t.match(false, isTwoCents("23.3"))
  t.match(false, isTwoCents(23.3))
  t.match(true, isTwoCents(23.34))
  t.match(true, isTwoCents("23.34"))
  t.end()
})

tap.test("isNumber checks correctly", t => {
  t.match(false, isNumber("banana"))
  t.match(false, isNumber("mango"))
  t.match(false, isNumber("   34.456"))
  t.match(false, isNumber("34.456   "))
  t.match(false, isNumber("34 .456"))
  t.match(false, isNumber("$34.45"))

  t.match(true, isNumber("34.456"))
  t.match(true, isNumber(34.456))
  t.match(true, isNumber(.456))
  t.match(true, isNumber(.456))
  t.match(true, isNumber(1))
  t.match(true, isNumber(1.))
  t.match(null, isNumber())
  t.end()
})

tap.test("validMoney returns a valid value for a money input", t => {
  t.match("", validMoney(""))
  t.match("54.32", validMoney(54.32))
  t.match("54.32", validMoney("54.32"))
  t.match(false, validMoney("banana"))
  t.end()
})

