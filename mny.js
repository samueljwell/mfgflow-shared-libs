import currency from "currency.js"

/*
 * Get a two cent number value for a given amount
 */
export const money = amt => currency(amt).value
export const format = (amt, stuff) => currency(amt, stuff)

/*
 * Get an empty value, an in progress number value, from the value of a money input
 *
 * Ex: "1" -> "1"
 * Ex: "1.1" -> "1.1"
 * Ex: "1.111" -> "1.11"
 * Ex: "" -> ""
 * Ex: "what" -> false
 */
export const validMoney = val => {
  const itsaNumber = isNumber(val)
  if (itsaNumber === false) return false
  if (itsaNumber === null) return val
  const newVal = isTwoCents(val) ? twoCents(val) : val
  return newVal
}

export const gte = (val, min) => Number(val) >= min ? Number(val) : min

export const addMoney = (...args) => {
  let total = 0
  for (let num of args) {
    total = currency(total).add(currency(num)).value
  }
  return total
}

export const multiplyMoney = (...args) => {
  let total = 1
  for (let num of args) {
    total = currency(total).multiply(currency(num)).value
  }
  return total
}

/*
 * Does amt have >= 2 cents
 *
 * Ex: 2.2 -> false
 * Ex: "2" -> false
 * Ex: "2.3" -> false
 * Ex: 2.23 -> true
 * Ex: "2.23" -> true
 * Ex: 2.234 -> true
 * Ex: "2.234" -> true
 */
export const isTwoCents = amt => {
  if (amt === "") return amt
  const splitty = amt.toString().split('.') 
  const afterDec = splitty[1]
  if (!afterDec) return false
  const hasCent = afterDec.charAt(1)
  return hasCent ? true : false
}

/*
 * Chop off remaining decimals after the second place
 */
export const twoCents = amt => {
  const splitty = amt.toString().split('.') 
  return splitty[0] + '.' + splitty[1].slice(0,2)
}

export const isNumber = amt => {
  if (!amt || amt === "") return null
  const re = /^\d*\.?\d*$/
  const str = amt.toString()
  return re.test(str)
}
